package example

import (
	"os"
	"testing"

	"github.com/gopherjs/gopherjs/js"

	"github.com/nyarla/gojs-milkcocoa"
)

func TestDataStore(t *testing.T) {
	endpoint := os.Getenv(`MILKCOCOA_ENDPOINT`)

	if endpoint == `` {
		t.Fatal(`this example test is required env value of 'MILKCOCOA_ENDPOINT'.`)
	}

	js.Global.Call(`eval`, `MilkCocoa = require('milkcocoa')`)

	app := milkcocoa.New(endpoint)
	dataStore := app.DataStore(`test`)
	dataSet := milkcocoa.DataStoreValue{
		`hello`: "world",
	}

	type result struct {
		err error
		ret *milkcocoa.DataStoreResult
	}

	// test for push
	recv := make(chan result, 1)
	dataStore.On(milkcocoa.EventPush, func(ret *milkcocoa.DataStoreResult, err error) {
		recv <- result{err: err, ret: ret}
	})

	dataStore.Push(dataSet)

	retval := <-recv

	if retval.err != nil {
		t.Fatal(retval.err)
	}

	if retval.ret.Id == `` {
		t.Fatal(`failed to push data: 'id' field not found in datastore result`, retval.ret)
	}

	if val, ok := retval.ret.Value[`hello`]; !ok {
		t.Fatal(`failed to push data: 'hello' field is not found in values of datasotre result: `, retval.ret)
	} else {
		if str, ok := val.(string); !ok {
			t.Fatal(`failed to push data: cannot convert 'hello' field value to string: `, val)
		} else {
			if str != `world` {
				t.Fatalf(`failed to push data: value of 'hello' is wrong: got: %s, expected: %v`, str, `world`)
			}
		}
	}
	dataStore.Off(milkcocoa.EventPush)

	// test for remove
	dataStore.On(milkcocoa.EventRemove, func(ret *milkcocoa.DataStoreResult, err error) {
		recv <- result{err: err, ret: ret}
	})

	dataStore.Remove(retval.ret.Id)

	retval = <-recv
	if retval.err != nil {
		t.Fatal(retval.err)
	}

	dataStore.Off(milkcocoa.EventRemove)

	// test for set
	dataStore.On(milkcocoa.EventSet, func(ret *milkcocoa.DataStoreResult, err error) {
		recv <- result{err: err, ret: ret}
	})

	dataStore.Set(`hello`, milkcocoa.DataStoreValue{`message`: "hi"})

	retval = <-recv
	if retval.err != nil {
		t.Fatal(retval.err)
	}

	if val, ok := retval.ret.Value[`message`]; !ok {
		t.Fatal(`failed to set data: 'message' field is not found in datastore result.`, retval.ret)
	} else {
		if str, ok := val.(string); !ok {
			t.Fatal(`failed to set data: cannot convert 'message' field value to string.`, val)
		} else {
			if str != `hi` {
				t.Fatalf(`failed to set data: value of 'message' is wrong: got: %v, expected: %v`, str, `hi`)
			}
		}
	}

	dataStore.Off(milkcocoa.EventSet)

	// test for get
	if retval, err := dataStore.Get(`hello`); err != nil {
		t.Fatal(err)
	} else {
		if val, ok := retval.Value[`message`]; !ok {
			t.Fatal(`failed to get data: 'message' field is not found in datastore result.`, retval)
		} else {
			if str, ok := val.(string); !ok {
				t.Fatal(`failed to get data: cannot convert 'message' field value to string.`, val)
			} else {
				if str != `hi` {
					t.Fatalf(`failed to get data: value of 'message' is wrong: got: %v, expected: %v`, str, `hi`)
				}
			}
		}
	}

	// test for send
	dataStore.On(milkcocoa.EventSend, func(ret *milkcocoa.DataStoreResult, err error) {
		recv <- result{err: err, ret: ret}
	})

	dataStore.Send(milkcocoa.DataStoreValue{
		`message`: "hello world!",
	})

	retval = <-recv
	if retval.err != nil {
		t.Fatal(retval.err)
	}

	if val, ok := retval.ret.Value[`message`]; !ok {
		t.Fatal(`failed to recieve data: 'message' field is not found in recieved result.`, retval.ret)
	} else {
		if str, ok := val.(string); !ok {
			t.Fatal(`failed to reciecve data: cannot convert 'message' field value to string.`, val)
		} else {
			if str != `hello world!` {
				t.Fatalf(`failed to get data: value of 'message' is wrong: got: %v, expected: %v`, str, `hello world!`)
			}
		}
	}

	// test for stream
	if ary, err := dataStore.Stream().Execute(); err != nil {
		t.Fatal(err)
	} else {
		for _, val := range ary {
			dataStore.Remove(val.Id)
		}
	}
}
