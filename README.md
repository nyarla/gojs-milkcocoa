milkcocoa
=========

A [Milkcocoa](https://mlkcca.com/) node.js SDK binding for [GopherJS](https://github.com/gopherjs/gopherjs)

DOCUMENTATION
-------------

<https://godoc.org/github.com/nyarla/gojs-milkcocoa>

RUNNING TEST REQUIREMENTS
-------------------------

  1. Running test with node.js, this test is required of node.js syscall integration for GopherJS
    * SEE ALSO: <https://github.com/gopherjs/gopherjs/blob/master/doc/syscalls.md>
  2. Running test for this library is required of `MIKLCOCOA_ENDPOINT` environment variable.
    * you can look at this value on [Milkcocoa](https://mlkcca.com/)'s dashboard
  3. Running test for this library is required of run `npm install .` command at before running this library test.

RUNNING TEST
------------

```bash
# in this library repository:
$ cd test
$ npm install . # install Milkcocoa's node.js SDK
$ MILKCOCOA_ENDPOINT="{your-application-id}.mlkcca.com" make test
```

LICENSE NOTICE
--------------

[Milkcocoa](https://mlkcca.com/) node.js SDK is uploaded at [npmjs.org](https://www.npmjs.com/package/milkcocoa) by milkcocoa team,
But this SDK does not have any license notice.

So I can't licensed this library, Because license of node.js SDK is unknown,
And this library is linked at node.js or Javascript SDK with GopherJS.

AUTHOR
------

Naoki OKAMURA a.k.a nyarla <nyarla@thotep.net>
