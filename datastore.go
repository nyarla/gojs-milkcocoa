package milkcocoa

import (
	"github.com/gopherjs/gopherjs/js"
)

type DataStoreEventType string

func (e DataStoreEventType) String() string {
	return string(e)
}

const (
	EventPush   DataStoreEventType = `push`
	EventRemove DataStoreEventType = `remove`
	EventSet    DataStoreEventType = `set`
	EventSend   DataStoreEventType = `send`
)

type DataStoreValue map[string]interface{}

type DataStoreResult struct {
	Id    string
	Path  string
	Value DataStoreValue
}

type jsDataStoreResult struct {
	err, val *js.Object
}

func (r jsDataStoreResult) Return() (*DataStoreResult, error) {
	if r.err != nil {
		return nil, &js.Error{Object: r.err}
	}

	ret := new(DataStoreResult)
	if r.val != nil {
		for _, key := range js.Keys(r.val) {
			switch key {
			case "id":
				ret.Id = r.val.Get(key).String()
			case "path":
				ret.Path = r.val.Get(key).String()
			case "value":
				ret.Value = DataStoreValue(r.val.Get(key).Interface().(map[string]interface{}))
			}
		}
	}

	return ret, nil
}

type DataStore interface {
	Send(val DataStoreValue)
	Push(val DataStoreValue) (*DataStoreResult, error)
	Set(id string, val DataStoreValue)
	Get(id string) (*DataStoreResult, error)
	Remove(id string)
	On(ev DataStoreEventType, cb func(*DataStoreResult, error))
	Off(ev DataStoreEventType)
	Child(path string) DataStore
	Stream() Stream
}

type dataStore struct {
	object *js.Object
}

func (d *dataStore) Send(val DataStoreValue) {
	d.object.Call(`send`, val)
}

func (d *dataStore) Push(val DataStoreValue) (*DataStoreResult, error) {
	recv := make(chan jsDataStoreResult)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 2 {
				args = append(args, nil)
			}

			recv <- jsDataStoreResult{err: args[0], val: args[1]}
		}()
		return nil
	})

	d.object.Call(`push`, val, callback)

	return (<-recv).Return()
}

func (d *dataStore) Set(id string, val DataStoreValue) {
	d.object.Call(`set`, id, val)
}

func (d *dataStore) Get(id string) (*DataStoreResult, error) {
	recv := make(chan jsDataStoreResult)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 2 {
				args = append(args, nil)
			}

			recv <- jsDataStoreResult{err: args[0], val: args[1]}
		}()

		return nil
	})

	d.object.Call(`get`, id, callback)

	return (<-recv).Return()
}

func (d *dataStore) Remove(id string) {
	d.object.Call(`remove`, id)
}

func (d *dataStore) On(event DataStoreEventType, cb func(*DataStoreResult, error)) {
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 1 {
				args = append(args, nil)
			}

			cb((jsDataStoreResult{val: args[0]}).Return())
		}()

		return nil
	})

	d.object.Call(`on`, event.String(), callback)
}

func (d *dataStore) Off(event DataStoreEventType) {
	d.object.Call(`off`, event.String())
}

func (d *dataStore) Child(path string) DataStore {
	return &dataStore{object: d.object.Call(`child`, path)}
}

func (d *dataStore) Stream() Stream {
	return &stream{object: d.object.Call(`stream`)}
}
