package milkcocoa

import (
	"github.com/gopherjs/gopherjs/js"
)

type AccountResult map[string]interface{}

type jsAccountResult struct {
	err, val *js.Object
}

func (jsObj jsAccountResult) Return() (AccountResult, error) {
	if jsObj.err != nil {
		return nil, &js.Error{Object: jsObj.err}
	}

	if jsObj.val != nil {
		return AccountResult(jsObj.val.Interface().(map[string]interface{})), nil
	}

	return nil, nil
}

var JSMilkCocoaClass = `MilkCocoa`

type MilkCocoa interface {
	DataStore(path string) DataStore
	AuthWithToken(jwt string) (AccountResult, error)
	Current() (AccountResult, error)
	Logout() error
}

type milkCocoa struct {
	object *js.Object
}

func New(endpoint string) MilkCocoa {
	return &milkCocoa{object: js.Global.Get(JSMilkCocoaClass).New(endpoint)}
}

func (m *milkCocoa) DataStore(path string) DataStore {
	return &dataStore{object: m.object.Call(`dataStore`, path)}
}

func (m *milkCocoa) AuthWithToken(jwt string) (AccountResult, error) {
	recv := make(chan jsAccountResult, 1)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 2 {
				args = append(args, nil)
			}

			recv <- jsAccountResult{err: args[0], val: args[1]}
		}()

		return nil
	})

	m.object.Call(`authWithToken`, jwt, callback)

	return (<-recv).Return()
}

func (m *milkCocoa) Current() (AccountResult, error) {
	recv := make(chan jsAccountResult, 1)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 2 {
				args = append(args, nil)
			}

			recv <- jsAccountResult{err: args[0], val: args[1]}
		}()

		return nil
	})

	m.object.Call(`user`, callback)

	return (<-recv).Return()
}

func (m *milkCocoa) Logout() error {
	recv := make(chan error)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 1 {
				args = append(args, nil)
			}

			recv <- &js.Error{Object: args[0]}
		}()
		return nil
	})

	m.object.Call(`logout`, callback)

	return <-recv
}
