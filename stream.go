package milkcocoa

import (
	"github.com/gopherjs/gopherjs/js"
)

type StreamSortType string

func (s StreamSortType) String() string {
	return string(s)
}

const (
	SortAsc  StreamSortType = `asc`
	SortDesc StreamSortType = `desc`
)

type jsStreamResult struct {
	err, val *js.Object
}

func (jsObj jsStreamResult) Return() ([]*DataStoreResult, error) {
	if jsObj.err != nil {
		return nil, &js.Error{Object: jsObj.err}
	}

	ret := make([]*DataStoreResult, 0)
	for i := 0; i < jsObj.val.Length(); i++ {
		if data, err := (jsDataStoreResult{val: jsObj.val.Index(i)}).Return(); err != nil {
			return nil, err
		} else {
			ret = append(ret, data)
		}
	}

	return ret, nil
}

type Stream interface {
	Size(num uint64) Stream
	Sort(sort StreamSortType) Stream
	Execute() ([]*DataStoreResult, error)
}

type stream struct {
	object *js.Object
}

func (s *stream) Size(num uint64) Stream {
	return &stream{object: s.object.Call(`size`, num)}
}

func (s *stream) Sort(sort StreamSortType) Stream {
	return &stream{object: s.object.Call(`sort`, sort.String())}
}

func (s *stream) Execute() ([]*DataStoreResult, error) {
	recv := make(chan jsStreamResult, 1)
	callback := js.MakeFunc(func(_ *js.Object, args []*js.Object) interface{} {
		go func() {
			if len(args) < 2 {
				args = append(args, nil)
			}

			recv <- jsStreamResult{err: args[0], val: args[1]}
		}()

		return nil
	})

	s.object.Call(`next`, callback)

	return (<-recv).Return()
}
